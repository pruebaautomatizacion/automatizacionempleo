package empleochoucair.stepdefinitions;

import empleochoucair.questions.Comparacion;
import empleochoucair.tasks.Abrir;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import empleochoucair.tasks.Buscar;
import net.serenitybdd.screenplay.actors.OnlineCast;
import static org.hamcrest.Matchers.equalTo;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.*;

public class BuscarEmpleoStepDefinitions {
    @Before
    public void configuracionInicial(){
        setTheStage(new OnlineCast());
    }


    @Dado("^que quiero postularme a una oferta laboral$")
    public void queQuieroPostularmeAUnaOfertaLaboral() throws Exception {
        theActorCalled("Carolina").wasAbleTo(Abrir.laPaginaDeChoucair());
    }

    @Cuando("^ingreso a la pagina virtual de choucair y selecciono la que se ajusta a mi perfil$")
    public void ingresoALaPaginaVirtualDeChoucairYSeleccionoLaQueSeAjustaAMiPerfil() throws Exception {
        theActorInTheSpotlight().attemptsTo(Buscar.ElEmpleo());
    }

    @Entonces("^verifico que sea analista de pruebas$")
    public void verificoQueSeaAnalistaDePruebas() throws Exception {
        theActorInTheSpotlight().should(seeThat(Comparacion.NombreEmpleo(), equalTo("Analista de Pruebas Generalista")));
    }
}
