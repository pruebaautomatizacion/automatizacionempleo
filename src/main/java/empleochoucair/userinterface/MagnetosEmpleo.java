package empleochoucair.userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;


public class MagnetosEmpleo extends PageObject{
    public static final Target TEXTO_MAGNETO = Target.the("Texto En Choucair sabemos").located(By.xpath("//h2[@class='font-company-primary'][contains(text(),'En Choucair sabemos')]"));
    public static final Target BOTON_CONOCER_VACANTES = Target.the("Boton conocer nuestras vacantes").located(By.xpath("//a[@class='btn'][contains(text(),'Conoce')]"));



}
