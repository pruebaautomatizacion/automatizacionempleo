package empleochoucair.userinterface;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.List;

@DefaultUrl("https://www.choucairtesting.com/")

public class ChoucairHome extends PageObject{
    public static final Target MENU_EMPLEO = Target.the("Menu de empleo").located(By.id("menu-item-550"));

    public  static WebDriver Driver = Serenity.getWebdriverManager().getCurrentDriver();


}
