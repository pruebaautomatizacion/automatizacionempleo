package empleochoucair.userinterface;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class Empleos extends PageObject{
    public static final Target PREPARARSE = Target.the("Imagen Prepararse").located(By.xpath("//a[@href='#prepararse'][contains(text(),'Prepararse')]"));
    public static final Target BOTON_EMPLEO = Target.the("Boton direccion de empleos").located(By.className("elementor-button-text"));
    public static final Target BOTON_CONTINUAR = Target.the("Boton para continuar").located(By.xpath("//div[@Class='elementor-button-wrapper']/a/span/span[contains(text(),'Continuar')]"));





}
