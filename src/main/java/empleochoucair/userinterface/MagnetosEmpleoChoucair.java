package empleochoucair.userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;


public class MagnetosEmpleoChoucair extends PageObject{
    public static final Target AUXILIAR_DE_NOMINA = Target.the("Texto Auxiliar de Nomina").located(By.xpath("//h2[@class='font-company-primary'][contains(text(),'Auxiliar de Nomina')]"));
    public static final Target AUTOMATIZADOR = Target.the("Texto Automatizador").located(By.xpath("//h2[@class='font-company-primary'][contains(text(),'Automatizador')]"));
    public static final Target PRUEBAS_GENERALISTA = Target.the("Texto Pruebas Generalista").located(By.xpath("//h2[@class='font-company-primary'][contains(text(),'Pruebas Generalista')]"));



}
