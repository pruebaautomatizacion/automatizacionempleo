package empleochoucair.iteractions;

import empleochoucair.tasks.Buscar;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.Tasks;
import org.openqa.selenium.WebDriver;

import java.util.Set;

public class Cambiar implements Interaction {

    public  static WebDriver driver = Serenity.getWebdriverManager().getCurrentDriver();
    private String ulr;

    public  Cambiar(String url){
        this.ulr = url;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        handleMultipleWindows(this.ulr);
    }

    public static Cambiar PesanaPor(String url) {
        return Tasks.instrumented(Cambiar.class, url);
    }

    private static  void handleMultipleWindows(String windowTitle) {
        Set<String> windows = driver.getWindowHandles();

        for (String window : windows) {
            driver.switchTo().window(window);
            if (driver.getTitle().contains(windowTitle)) {
                return;
            }
        }
    }
}
