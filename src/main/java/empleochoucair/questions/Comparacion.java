package empleochoucair.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import static empleochoucair.userinterface.MagnetosEmpleoChoucair.PRUEBAS_GENERALISTA;

public class Comparacion implements Question {

    public static Comparacion NombreEmpleo(){
        return new Comparacion();
    }

   @Override
    public String answeredBy(Actor actor) {
        return Text.of(PRUEBAS_GENERALISTA).viewedBy(actor).asString();
    }
}
