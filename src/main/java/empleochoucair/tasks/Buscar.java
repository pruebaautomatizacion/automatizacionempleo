package empleochoucair.tasks;



import empleochoucair.iteractions.Cambiar;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Scroll;


import static empleochoucair.userinterface.ChoucairHome.MENU_EMPLEO;
import static empleochoucair.userinterface.Empleos.*;
import static empleochoucair.userinterface.MagnetosEmpleo.BOTON_CONOCER_VACANTES;
import static empleochoucair.userinterface.MagnetosEmpleo.TEXTO_MAGNETO;
import static empleochoucair.userinterface.MagnetosEmpleoChoucair.AUTOMATIZADOR;
import static empleochoucair.userinterface.MagnetosEmpleoChoucair.AUXILIAR_DE_NOMINA;

public class Buscar implements Task {


    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(
            Click.on(MENU_EMPLEO),
                Scroll.to(PREPARARSE),
                Click.on(BOTON_EMPLEO),
                Click.on(BOTON_CONTINUAR),
                Scroll.to(TEXTO_MAGNETO),
                Scroll.to(BOTON_CONOCER_VACANTES),
                Click.on(BOTON_CONOCER_VACANTES),
                Cambiar.PesanaPor("https://www.magneto365.com/es/empleos/choucair"),
                Scroll.to(AUXILIAR_DE_NOMINA),
                Scroll.to(AUTOMATIZADOR)



        );



    }
    public static Buscar ElEmpleo() {
        return Tasks.instrumented(Buscar.class);
    }


}
