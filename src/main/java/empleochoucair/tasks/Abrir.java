package empleochoucair.tasks;

import empleochoucair.userinterface.ChoucairHome;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Open;

public class Abrir implements Task {

    ChoucairHome ChoucairHome;
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Open.browserOn(ChoucairHome));
    }

    public static Abrir laPaginaDeChoucair() {
        return Tasks.instrumented(Abrir.class);
    }

}

